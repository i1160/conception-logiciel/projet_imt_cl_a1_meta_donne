/**
 */
package bank;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Current Account</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see bank.BankPackage#getCurrentAccount()
 * @model
 * @generated
 */
public interface CurrentAccount extends Account {
} // CurrentAccount
