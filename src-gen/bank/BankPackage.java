/**
 */
package bank;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see bank.BankFactory
 * @model kind="package"
 * @generated
 */
public interface BankPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "bank";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "bank";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "bank";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BankPackage eINSTANCE = bank.impl.BankPackageImpl.init();

	/**
	 * The meta object id for the '{@link bank.impl.BanksImpl <em>Banks</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see bank.impl.BanksImpl
	 * @see bank.impl.BankPackageImpl#getBanks()
	 * @generated
	 */
	int BANKS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANKS__NAME = 0;

	/**
	 * The feature id for the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANKS__ADDRESS = 1;

	/**
	 * The feature id for the '<em><b>Account Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANKS__ACCOUNT_COLLECTION = 2;

	/**
	 * The feature id for the '<em><b>Client Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANKS__CLIENT_COLLECTION = 3;

	/**
	 * The number of structural features of the '<em>Banks</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANKS_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Add Client</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANKS___ADD_CLIENT__CLIENT = 0;

	/**
	 * The operation id for the '<em>Remove Client</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANKS___REMOVE_CLIENT__CLIENT = 1;

	/**
	 * The operation id for the '<em>Add Account</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANKS___ADD_ACCOUNT__ACCOUNT = 2;

	/**
	 * The operation id for the '<em>Remove Account</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANKS___REMOVE_ACCOUNT__ACCOUNT = 3;

	/**
	 * The number of operations of the '<em>Banks</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BANKS_OPERATION_COUNT = 4;

	/**
	 * The meta object id for the '{@link bank.impl.AccountCollectionImpl <em>Account Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see bank.impl.AccountCollectionImpl
	 * @see bank.impl.BankPackageImpl#getAccountCollection()
	 * @generated
	 */
	int ACCOUNT_COLLECTION = 1;

	/**
	 * The feature id for the '<em><b>Accounts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_COLLECTION__ACCOUNTS = 0;

	/**
	 * The number of structural features of the '<em>Account Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_COLLECTION_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Add</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_COLLECTION___ADD__ACCOUNT = 0;

	/**
	 * The operation id for the '<em>Remove</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_COLLECTION___REMOVE__ACCOUNT = 1;

	/**
	 * The number of operations of the '<em>Account Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_COLLECTION_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link bank.impl.ClientCollectionImpl <em>Client Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see bank.impl.ClientCollectionImpl
	 * @see bank.impl.BankPackageImpl#getClientCollection()
	 * @generated
	 */
	int CLIENT_COLLECTION = 2;

	/**
	 * The feature id for the '<em><b>Clients</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_COLLECTION__CLIENTS = 0;

	/**
	 * The number of structural features of the '<em>Client Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_COLLECTION_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Add</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_COLLECTION___ADD__CLIENT = 0;

	/**
	 * The operation id for the '<em>Remove</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_COLLECTION___REMOVE__CLIENT = 1;

	/**
	 * The number of operations of the '<em>Client Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_COLLECTION_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link bank.impl.ClientImpl <em>Client</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see bank.impl.ClientImpl
	 * @see bank.impl.BankPackageImpl#getClient()
	 * @generated
	 */
	int CLIENT = 3;

	/**
	 * The feature id for the '<em><b>Total Balance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT__TOTAL_BALANCE = 0;

	/**
	 * The feature id for the '<em><b>Accounts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT__ACCOUNTS = 1;

	/**
	 * The feature id for the '<em><b>Client id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT__CLIENT_ID = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT__NAME = 3;

	/**
	 * The feature id for the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT__ADDRESS = 4;

	/**
	 * The feature id for the '<em><b>Date Of Birth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT__DATE_OF_BIRTH = 5;

	/**
	 * The number of structural features of the '<em>Client</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_FEATURE_COUNT = 6;

	/**
	 * The operation id for the '<em>Add Account</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT___ADD_ACCOUNT__ACCOUNT = 0;

	/**
	 * The operation id for the '<em>Remove Account</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT___REMOVE_ACCOUNT__ACCOUNT = 1;

	/**
	 * The number of operations of the '<em>Client</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLIENT_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link bank.impl.AccountImpl <em>Account</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see bank.impl.AccountImpl
	 * @see bank.impl.BankPackageImpl#getAccount()
	 * @generated
	 */
	int ACCOUNT = 4;

	/**
	 * The feature id for the '<em><b>Account Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__ACCOUNT_NUMBER = 0;

	/**
	 * The feature id for the '<em><b>Avaliable Balance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT__AVALIABLE_BALANCE = 1;

	/**
	 * The number of structural features of the '<em>Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Credit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT___CREDIT__DOUBLE = 0;

	/**
	 * The operation id for the '<em>Debit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT___DEBIT__DOUBLE = 1;

	/**
	 * The number of operations of the '<em>Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCOUNT_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link bank.impl.SavingAccountImpl <em>Saving Account</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see bank.impl.SavingAccountImpl
	 * @see bank.impl.BankPackageImpl#getSavingAccount()
	 * @generated
	 */
	int SAVING_ACCOUNT = 5;

	/**
	 * The feature id for the '<em><b>Account Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVING_ACCOUNT__ACCOUNT_NUMBER = ACCOUNT__ACCOUNT_NUMBER;

	/**
	 * The feature id for the '<em><b>Avaliable Balance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVING_ACCOUNT__AVALIABLE_BALANCE = ACCOUNT__AVALIABLE_BALANCE;

	/**
	 * The feature id for the '<em><b>Interest Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVING_ACCOUNT__INTEREST_RATE = ACCOUNT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Saving Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVING_ACCOUNT_FEATURE_COUNT = ACCOUNT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Credit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVING_ACCOUNT___CREDIT__DOUBLE = ACCOUNT___CREDIT__DOUBLE;

	/**
	 * The operation id for the '<em>Debit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVING_ACCOUNT___DEBIT__DOUBLE = ACCOUNT___DEBIT__DOUBLE;

	/**
	 * The operation id for the '<em>Interest Rate Calculation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVING_ACCOUNT___INTEREST_RATE_CALCULATION = ACCOUNT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Saving Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAVING_ACCOUNT_OPERATION_COUNT = ACCOUNT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link bank.impl.CurrentAccountImpl <em>Current Account</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see bank.impl.CurrentAccountImpl
	 * @see bank.impl.BankPackageImpl#getCurrentAccount()
	 * @generated
	 */
	int CURRENT_ACCOUNT = 6;

	/**
	 * The feature id for the '<em><b>Account Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CURRENT_ACCOUNT__ACCOUNT_NUMBER = ACCOUNT__ACCOUNT_NUMBER;

	/**
	 * The feature id for the '<em><b>Avaliable Balance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CURRENT_ACCOUNT__AVALIABLE_BALANCE = ACCOUNT__AVALIABLE_BALANCE;

	/**
	 * The number of structural features of the '<em>Current Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CURRENT_ACCOUNT_FEATURE_COUNT = ACCOUNT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Credit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CURRENT_ACCOUNT___CREDIT__DOUBLE = ACCOUNT___CREDIT__DOUBLE;

	/**
	 * The operation id for the '<em>Debit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CURRENT_ACCOUNT___DEBIT__DOUBLE = ACCOUNT___DEBIT__DOUBLE;

	/**
	 * The number of operations of the '<em>Current Account</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CURRENT_ACCOUNT_OPERATION_COUNT = ACCOUNT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link bank.Banks <em>Banks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Banks</em>'.
	 * @see bank.Banks
	 * @generated
	 */
	EClass getBanks();

	/**
	 * Returns the meta object for the attribute '{@link bank.Banks#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see bank.Banks#getName()
	 * @see #getBanks()
	 * @generated
	 */
	EAttribute getBanks_Name();

	/**
	 * Returns the meta object for the attribute '{@link bank.Banks#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address</em>'.
	 * @see bank.Banks#getAddress()
	 * @see #getBanks()
	 * @generated
	 */
	EAttribute getBanks_Address();

	/**
	 * Returns the meta object for the containment reference '{@link bank.Banks#getAccountCollection <em>Account Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Account Collection</em>'.
	 * @see bank.Banks#getAccountCollection()
	 * @see #getBanks()
	 * @generated
	 */
	EReference getBanks_AccountCollection();

	/**
	 * Returns the meta object for the containment reference '{@link bank.Banks#getClientCollection <em>Client Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Client Collection</em>'.
	 * @see bank.Banks#getClientCollection()
	 * @see #getBanks()
	 * @generated
	 */
	EReference getBanks_ClientCollection();

	/**
	 * Returns the meta object for the '{@link bank.Banks#addClient(bank.Client) <em>Add Client</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Client</em>' operation.
	 * @see bank.Banks#addClient(bank.Client)
	 * @generated
	 */
	EOperation getBanks__AddClient__Client();

	/**
	 * Returns the meta object for the '{@link bank.Banks#removeClient(bank.Client) <em>Remove Client</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Client</em>' operation.
	 * @see bank.Banks#removeClient(bank.Client)
	 * @generated
	 */
	EOperation getBanks__RemoveClient__Client();

	/**
	 * Returns the meta object for the '{@link bank.Banks#addAccount(bank.Account) <em>Add Account</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Account</em>' operation.
	 * @see bank.Banks#addAccount(bank.Account)
	 * @generated
	 */
	EOperation getBanks__AddAccount__Account();

	/**
	 * Returns the meta object for the '{@link bank.Banks#removeAccount(bank.Account) <em>Remove Account</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Account</em>' operation.
	 * @see bank.Banks#removeAccount(bank.Account)
	 * @generated
	 */
	EOperation getBanks__RemoveAccount__Account();

	/**
	 * Returns the meta object for class '{@link bank.AccountCollection <em>Account Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Account Collection</em>'.
	 * @see bank.AccountCollection
	 * @generated
	 */
	EClass getAccountCollection();

	/**
	 * Returns the meta object for the containment reference list '{@link bank.AccountCollection#getAccounts <em>Accounts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Accounts</em>'.
	 * @see bank.AccountCollection#getAccounts()
	 * @see #getAccountCollection()
	 * @generated
	 */
	EReference getAccountCollection_Accounts();

	/**
	 * Returns the meta object for the '{@link bank.AccountCollection#add(bank.Account) <em>Add</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add</em>' operation.
	 * @see bank.AccountCollection#add(bank.Account)
	 * @generated
	 */
	EOperation getAccountCollection__Add__Account();

	/**
	 * Returns the meta object for the '{@link bank.AccountCollection#remove(bank.Account) <em>Remove</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove</em>' operation.
	 * @see bank.AccountCollection#remove(bank.Account)
	 * @generated
	 */
	EOperation getAccountCollection__Remove__Account();

	/**
	 * Returns the meta object for class '{@link bank.ClientCollection <em>Client Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Client Collection</em>'.
	 * @see bank.ClientCollection
	 * @generated
	 */
	EClass getClientCollection();

	/**
	 * Returns the meta object for the containment reference list '{@link bank.ClientCollection#getClients <em>Clients</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Clients</em>'.
	 * @see bank.ClientCollection#getClients()
	 * @see #getClientCollection()
	 * @generated
	 */
	EReference getClientCollection_Clients();

	/**
	 * Returns the meta object for the '{@link bank.ClientCollection#add(bank.Client) <em>Add</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add</em>' operation.
	 * @see bank.ClientCollection#add(bank.Client)
	 * @generated
	 */
	EOperation getClientCollection__Add__Client();

	/**
	 * Returns the meta object for the '{@link bank.ClientCollection#remove(bank.Client) <em>Remove</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove</em>' operation.
	 * @see bank.ClientCollection#remove(bank.Client)
	 * @generated
	 */
	EOperation getClientCollection__Remove__Client();

	/**
	 * Returns the meta object for class '{@link bank.Client <em>Client</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Client</em>'.
	 * @see bank.Client
	 * @generated
	 */
	EClass getClient();

	/**
	 * Returns the meta object for the attribute '{@link bank.Client#getTotalBalance <em>Total Balance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total Balance</em>'.
	 * @see bank.Client#getTotalBalance()
	 * @see #getClient()
	 * @generated
	 */
	EAttribute getClient_TotalBalance();

	/**
	 * Returns the meta object for the containment reference list '{@link bank.Client#getAccounts <em>Accounts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Accounts</em>'.
	 * @see bank.Client#getAccounts()
	 * @see #getClient()
	 * @generated
	 */
	EReference getClient_Accounts();

	/**
	 * Returns the meta object for the attribute '{@link bank.Client#getClient_id <em>Client id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Client id</em>'.
	 * @see bank.Client#getClient_id()
	 * @see #getClient()
	 * @generated
	 */
	EAttribute getClient_Client_id();

	/**
	 * Returns the meta object for the attribute '{@link bank.Client#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see bank.Client#getName()
	 * @see #getClient()
	 * @generated
	 */
	EAttribute getClient_Name();

	/**
	 * Returns the meta object for the attribute '{@link bank.Client#getAddress <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Address</em>'.
	 * @see bank.Client#getAddress()
	 * @see #getClient()
	 * @generated
	 */
	EAttribute getClient_Address();

	/**
	 * Returns the meta object for the attribute '{@link bank.Client#getDateOfBirth <em>Date Of Birth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date Of Birth</em>'.
	 * @see bank.Client#getDateOfBirth()
	 * @see #getClient()
	 * @generated
	 */
	EAttribute getClient_DateOfBirth();

	/**
	 * Returns the meta object for the '{@link bank.Client#addAccount(bank.Account) <em>Add Account</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Account</em>' operation.
	 * @see bank.Client#addAccount(bank.Account)
	 * @generated
	 */
	EOperation getClient__AddAccount__Account();

	/**
	 * Returns the meta object for the '{@link bank.Client#removeAccount(bank.Account) <em>Remove Account</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Account</em>' operation.
	 * @see bank.Client#removeAccount(bank.Account)
	 * @generated
	 */
	EOperation getClient__RemoveAccount__Account();

	/**
	 * Returns the meta object for class '{@link bank.Account <em>Account</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Account</em>'.
	 * @see bank.Account
	 * @generated
	 */
	EClass getAccount();

	/**
	 * Returns the meta object for the attribute '{@link bank.Account#getAccountNumber <em>Account Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Account Number</em>'.
	 * @see bank.Account#getAccountNumber()
	 * @see #getAccount()
	 * @generated
	 */
	EAttribute getAccount_AccountNumber();

	/**
	 * Returns the meta object for the attribute '{@link bank.Account#getAvaliableBalance <em>Avaliable Balance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avaliable Balance</em>'.
	 * @see bank.Account#getAvaliableBalance()
	 * @see #getAccount()
	 * @generated
	 */
	EAttribute getAccount_AvaliableBalance();

	/**
	 * Returns the meta object for the '{@link bank.Account#credit(double) <em>Credit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Credit</em>' operation.
	 * @see bank.Account#credit(double)
	 * @generated
	 */
	EOperation getAccount__Credit__double();

	/**
	 * Returns the meta object for the '{@link bank.Account#debit(double) <em>Debit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Debit</em>' operation.
	 * @see bank.Account#debit(double)
	 * @generated
	 */
	EOperation getAccount__Debit__double();

	/**
	 * Returns the meta object for class '{@link bank.SavingAccount <em>Saving Account</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Saving Account</em>'.
	 * @see bank.SavingAccount
	 * @generated
	 */
	EClass getSavingAccount();

	/**
	 * Returns the meta object for the attribute '{@link bank.SavingAccount#getInterestRate <em>Interest Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interest Rate</em>'.
	 * @see bank.SavingAccount#getInterestRate()
	 * @see #getSavingAccount()
	 * @generated
	 */
	EAttribute getSavingAccount_InterestRate();

	/**
	 * Returns the meta object for the '{@link bank.SavingAccount#interestRateCalculation() <em>Interest Rate Calculation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Interest Rate Calculation</em>' operation.
	 * @see bank.SavingAccount#interestRateCalculation()
	 * @generated
	 */
	EOperation getSavingAccount__InterestRateCalculation();

	/**
	 * Returns the meta object for class '{@link bank.CurrentAccount <em>Current Account</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Current Account</em>'.
	 * @see bank.CurrentAccount
	 * @generated
	 */
	EClass getCurrentAccount();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BankFactory getBankFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link bank.impl.BanksImpl <em>Banks</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see bank.impl.BanksImpl
		 * @see bank.impl.BankPackageImpl#getBanks()
		 * @generated
		 */
		EClass BANKS = eINSTANCE.getBanks();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BANKS__NAME = eINSTANCE.getBanks_Name();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BANKS__ADDRESS = eINSTANCE.getBanks_Address();

		/**
		 * The meta object literal for the '<em><b>Account Collection</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BANKS__ACCOUNT_COLLECTION = eINSTANCE.getBanks_AccountCollection();

		/**
		 * The meta object literal for the '<em><b>Client Collection</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BANKS__CLIENT_COLLECTION = eINSTANCE.getBanks_ClientCollection();

		/**
		 * The meta object literal for the '<em><b>Add Client</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANKS___ADD_CLIENT__CLIENT = eINSTANCE.getBanks__AddClient__Client();

		/**
		 * The meta object literal for the '<em><b>Remove Client</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANKS___REMOVE_CLIENT__CLIENT = eINSTANCE.getBanks__RemoveClient__Client();

		/**
		 * The meta object literal for the '<em><b>Add Account</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANKS___ADD_ACCOUNT__ACCOUNT = eINSTANCE.getBanks__AddAccount__Account();

		/**
		 * The meta object literal for the '<em><b>Remove Account</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BANKS___REMOVE_ACCOUNT__ACCOUNT = eINSTANCE.getBanks__RemoveAccount__Account();

		/**
		 * The meta object literal for the '{@link bank.impl.AccountCollectionImpl <em>Account Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see bank.impl.AccountCollectionImpl
		 * @see bank.impl.BankPackageImpl#getAccountCollection()
		 * @generated
		 */
		EClass ACCOUNT_COLLECTION = eINSTANCE.getAccountCollection();

		/**
		 * The meta object literal for the '<em><b>Accounts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACCOUNT_COLLECTION__ACCOUNTS = eINSTANCE.getAccountCollection_Accounts();

		/**
		 * The meta object literal for the '<em><b>Add</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT_COLLECTION___ADD__ACCOUNT = eINSTANCE.getAccountCollection__Add__Account();

		/**
		 * The meta object literal for the '<em><b>Remove</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT_COLLECTION___REMOVE__ACCOUNT = eINSTANCE.getAccountCollection__Remove__Account();

		/**
		 * The meta object literal for the '{@link bank.impl.ClientCollectionImpl <em>Client Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see bank.impl.ClientCollectionImpl
		 * @see bank.impl.BankPackageImpl#getClientCollection()
		 * @generated
		 */
		EClass CLIENT_COLLECTION = eINSTANCE.getClientCollection();

		/**
		 * The meta object literal for the '<em><b>Clients</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLIENT_COLLECTION__CLIENTS = eINSTANCE.getClientCollection_Clients();

		/**
		 * The meta object literal for the '<em><b>Add</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CLIENT_COLLECTION___ADD__CLIENT = eINSTANCE.getClientCollection__Add__Client();

		/**
		 * The meta object literal for the '<em><b>Remove</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CLIENT_COLLECTION___REMOVE__CLIENT = eINSTANCE.getClientCollection__Remove__Client();

		/**
		 * The meta object literal for the '{@link bank.impl.ClientImpl <em>Client</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see bank.impl.ClientImpl
		 * @see bank.impl.BankPackageImpl#getClient()
		 * @generated
		 */
		EClass CLIENT = eINSTANCE.getClient();

		/**
		 * The meta object literal for the '<em><b>Total Balance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT__TOTAL_BALANCE = eINSTANCE.getClient_TotalBalance();

		/**
		 * The meta object literal for the '<em><b>Accounts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLIENT__ACCOUNTS = eINSTANCE.getClient_Accounts();

		/**
		 * The meta object literal for the '<em><b>Client id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT__CLIENT_ID = eINSTANCE.getClient_Client_id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT__NAME = eINSTANCE.getClient_Name();

		/**
		 * The meta object literal for the '<em><b>Address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT__ADDRESS = eINSTANCE.getClient_Address();

		/**
		 * The meta object literal for the '<em><b>Date Of Birth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLIENT__DATE_OF_BIRTH = eINSTANCE.getClient_DateOfBirth();

		/**
		 * The meta object literal for the '<em><b>Add Account</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CLIENT___ADD_ACCOUNT__ACCOUNT = eINSTANCE.getClient__AddAccount__Account();

		/**
		 * The meta object literal for the '<em><b>Remove Account</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CLIENT___REMOVE_ACCOUNT__ACCOUNT = eINSTANCE.getClient__RemoveAccount__Account();

		/**
		 * The meta object literal for the '{@link bank.impl.AccountImpl <em>Account</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see bank.impl.AccountImpl
		 * @see bank.impl.BankPackageImpl#getAccount()
		 * @generated
		 */
		EClass ACCOUNT = eINSTANCE.getAccount();

		/**
		 * The meta object literal for the '<em><b>Account Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCOUNT__ACCOUNT_NUMBER = eINSTANCE.getAccount_AccountNumber();

		/**
		 * The meta object literal for the '<em><b>Avaliable Balance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCOUNT__AVALIABLE_BALANCE = eINSTANCE.getAccount_AvaliableBalance();

		/**
		 * The meta object literal for the '<em><b>Credit</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT___CREDIT__DOUBLE = eINSTANCE.getAccount__Credit__double();

		/**
		 * The meta object literal for the '<em><b>Debit</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACCOUNT___DEBIT__DOUBLE = eINSTANCE.getAccount__Debit__double();

		/**
		 * The meta object literal for the '{@link bank.impl.SavingAccountImpl <em>Saving Account</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see bank.impl.SavingAccountImpl
		 * @see bank.impl.BankPackageImpl#getSavingAccount()
		 * @generated
		 */
		EClass SAVING_ACCOUNT = eINSTANCE.getSavingAccount();

		/**
		 * The meta object literal for the '<em><b>Interest Rate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SAVING_ACCOUNT__INTEREST_RATE = eINSTANCE.getSavingAccount_InterestRate();

		/**
		 * The meta object literal for the '<em><b>Interest Rate Calculation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SAVING_ACCOUNT___INTEREST_RATE_CALCULATION = eINSTANCE.getSavingAccount__InterestRateCalculation();

		/**
		 * The meta object literal for the '{@link bank.impl.CurrentAccountImpl <em>Current Account</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see bank.impl.CurrentAccountImpl
		 * @see bank.impl.BankPackageImpl#getCurrentAccount()
		 * @generated
		 */
		EClass CURRENT_ACCOUNT = eINSTANCE.getCurrentAccount();

	}

} //BankPackage
