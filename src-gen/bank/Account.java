/**
 */
package bank;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Account</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bank.Account#getAccountNumber <em>Account Number</em>}</li>
 *   <li>{@link bank.Account#getAvaliableBalance <em>Avaliable Balance</em>}</li>
 * </ul>
 *
 * @see bank.BankPackage#getAccount()
 * @model abstract="true"
 * @generated
 */
public interface Account extends EObject {
	/**
	 * Returns the value of the '<em><b>Account Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Account Number</em>' attribute.
	 * @see #setAccountNumber(int)
	 * @see bank.BankPackage#getAccount_AccountNumber()
	 * @model unique="false"
	 * @generated
	 */
	int getAccountNumber();

	/**
	 * Sets the value of the '{@link bank.Account#getAccountNumber <em>Account Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Account Number</em>' attribute.
	 * @see #getAccountNumber()
	 * @generated
	 */
	void setAccountNumber(int value);

	/**
	 * Returns the value of the '<em><b>Avaliable Balance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avaliable Balance</em>' attribute.
	 * @see #setAvaliableBalance(double)
	 * @see bank.BankPackage#getAccount_AvaliableBalance()
	 * @model unique="false"
	 * @generated
	 */
	double getAvaliableBalance();

	/**
	 * Sets the value of the '{@link bank.Account#getAvaliableBalance <em>Avaliable Balance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avaliable Balance</em>' attribute.
	 * @see #getAvaliableBalance()
	 * @generated
	 */
	void setAvaliableBalance(double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model amountUnique="false"
	 * @generated
	 */
	void credit(double amount);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model amountUnique="false"
	 * @generated
	 */
	void debit(double amount);

} // Account
