/**
 */
package bank;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Saving Account</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bank.SavingAccount#getInterestRate <em>Interest Rate</em>}</li>
 * </ul>
 *
 * @see bank.BankPackage#getSavingAccount()
 * @model
 * @generated
 */
public interface SavingAccount extends Account {
	/**
	 * Returns the value of the '<em><b>Interest Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interest Rate</em>' attribute.
	 * @see #setInterestRate(double)
	 * @see bank.BankPackage#getSavingAccount_InterestRate()
	 * @model unique="false"
	 * @generated
	 */
	double getInterestRate();

	/**
	 * Sets the value of the '{@link bank.SavingAccount#getInterestRate <em>Interest Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interest Rate</em>' attribute.
	 * @see #getInterestRate()
	 * @generated
	 */
	void setInterestRate(double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false"
	 * @generated
	 */
	double interestRateCalculation();

} // SavingAccount
