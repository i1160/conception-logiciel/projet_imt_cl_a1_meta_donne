/**
 */
package bank;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Banks</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bank.Banks#getName <em>Name</em>}</li>
 *   <li>{@link bank.Banks#getAddress <em>Address</em>}</li>
 *   <li>{@link bank.Banks#getAccountCollection <em>Account Collection</em>}</li>
 *   <li>{@link bank.Banks#getClientCollection <em>Client Collection</em>}</li>
 * </ul>
 *
 * @see bank.BankPackage#getBanks()
 * @model
 * @generated
 */
public interface Banks extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see bank.BankPackage#getBanks_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link bank.Banks#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address</em>' attribute.
	 * @see #setAddress(String)
	 * @see bank.BankPackage#getBanks_Address()
	 * @model unique="false"
	 * @generated
	 */
	String getAddress();

	/**
	 * Sets the value of the '{@link bank.Banks#getAddress <em>Address</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address</em>' attribute.
	 * @see #getAddress()
	 * @generated
	 */
	void setAddress(String value);

	/**
	 * Returns the value of the '<em><b>Account Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Account Collection</em>' containment reference.
	 * @see #setAccountCollection(AccountCollection)
	 * @see bank.BankPackage#getBanks_AccountCollection()
	 * @model containment="true"
	 * @generated
	 */
	AccountCollection getAccountCollection();

	/**
	 * Sets the value of the '{@link bank.Banks#getAccountCollection <em>Account Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Account Collection</em>' containment reference.
	 * @see #getAccountCollection()
	 * @generated
	 */
	void setAccountCollection(AccountCollection value);

	/**
	 * Returns the value of the '<em><b>Client Collection</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Client Collection</em>' containment reference.
	 * @see #setClientCollection(ClientCollection)
	 * @see bank.BankPackage#getBanks_ClientCollection()
	 * @model containment="true"
	 * @generated
	 */
	ClientCollection getClientCollection();

	/**
	 * Sets the value of the '{@link bank.Banks#getClientCollection <em>Client Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Client Collection</em>' containment reference.
	 * @see #getClientCollection()
	 * @generated
	 */
	void setClientCollection(ClientCollection value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void addClient(Client c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void removeClient(Client c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void addAccount(Account a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void removeAccount(Account a);

} // Banks
