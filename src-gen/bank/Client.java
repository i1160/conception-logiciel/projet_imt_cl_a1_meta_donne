/**
 */
package bank;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Client</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bank.Client#getTotalBalance <em>Total Balance</em>}</li>
 *   <li>{@link bank.Client#getAccounts <em>Accounts</em>}</li>
 *   <li>{@link bank.Client#getClient_id <em>Client id</em>}</li>
 *   <li>{@link bank.Client#getName <em>Name</em>}</li>
 *   <li>{@link bank.Client#getAddress <em>Address</em>}</li>
 *   <li>{@link bank.Client#getDateOfBirth <em>Date Of Birth</em>}</li>
 * </ul>
 *
 * @see bank.BankPackage#getClient()
 * @model
 * @generated
 */
public interface Client extends EObject {
	/**
	 * Returns the value of the '<em><b>Total Balance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Balance</em>' attribute.
	 * @see #setTotalBalance(double)
	 * @see bank.BankPackage#getClient_TotalBalance()
	 * @model unique="false"
	 * @generated
	 */
	double getTotalBalance();

	/**
	 * Sets the value of the '{@link bank.Client#getTotalBalance <em>Total Balance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Balance</em>' attribute.
	 * @see #getTotalBalance()
	 * @generated
	 */
	void setTotalBalance(double value);

	/**
	 * Returns the value of the '<em><b>Accounts</b></em>' containment reference list.
	 * The list contents are of type {@link bank.Account}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accounts</em>' containment reference list.
	 * @see bank.BankPackage#getClient_Accounts()
	 * @model containment="true"
	 * @generated
	 */
	EList<Account> getAccounts();

	/**
	 * Returns the value of the '<em><b>Client id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Client id</em>' attribute.
	 * @see #setClient_id(int)
	 * @see bank.BankPackage#getClient_Client_id()
	 * @model unique="false"
	 * @generated
	 */
	int getClient_id();

	/**
	 * Sets the value of the '{@link bank.Client#getClient_id <em>Client id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Client id</em>' attribute.
	 * @see #getClient_id()
	 * @generated
	 */
	void setClient_id(int value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see bank.BankPackage#getClient_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link bank.Client#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Address</em>' attribute.
	 * @see #setAddress(String)
	 * @see bank.BankPackage#getClient_Address()
	 * @model unique="false"
	 * @generated
	 */
	String getAddress();

	/**
	 * Sets the value of the '{@link bank.Client#getAddress <em>Address</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Address</em>' attribute.
	 * @see #getAddress()
	 * @generated
	 */
	void setAddress(String value);

	/**
	 * Returns the value of the '<em><b>Date Of Birth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date Of Birth</em>' attribute.
	 * @see #setDateOfBirth(Date)
	 * @see bank.BankPackage#getClient_DateOfBirth()
	 * @model unique="false"
	 * @generated
	 */
	Date getDateOfBirth();

	/**
	 * Sets the value of the '{@link bank.Client#getDateOfBirth <em>Date Of Birth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date Of Birth</em>' attribute.
	 * @see #getDateOfBirth()
	 * @generated
	 */
	void setDateOfBirth(Date value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void addAccount(Account a);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aUnique="false"
	 * @generated
	 */
	void removeAccount(Account a);

} // Client
