/**
 */
package bank.impl;

import bank.Account;
import bank.AccountCollection;
import bank.BankFactory;
import bank.BankPackage;
import bank.Banks;
import bank.Client;
import bank.ClientCollection;
import bank.CurrentAccount;
import bank.SavingAccount;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BankPackageImpl extends EPackageImpl implements BankPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass banksEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accountCollectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clientCollectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clientEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass accountEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass savingAccountEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass currentAccountEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see bank.BankPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BankPackageImpl() {
		super(eNS_URI, BankFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link BankPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BankPackage init() {
		if (isInited) return (BankPackage)EPackage.Registry.INSTANCE.getEPackage(BankPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredBankPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		BankPackageImpl theBankPackage = registeredBankPackage instanceof BankPackageImpl ? (BankPackageImpl)registeredBankPackage : new BankPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theBankPackage.createPackageContents();

		// Initialize created meta-data
		theBankPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBankPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BankPackage.eNS_URI, theBankPackage);
		return theBankPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBanks() {
		return banksEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBanks_Name() {
		return (EAttribute)banksEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBanks_Address() {
		return (EAttribute)banksEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBanks_AccountCollection() {
		return (EReference)banksEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBanks_ClientCollection() {
		return (EReference)banksEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBanks__AddClient__Client() {
		return banksEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBanks__RemoveClient__Client() {
		return banksEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBanks__AddAccount__Account() {
		return banksEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBanks__RemoveAccount__Account() {
		return banksEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccountCollection() {
		return accountCollectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAccountCollection_Accounts() {
		return (EReference)accountCollectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccountCollection__Add__Account() {
		return accountCollectionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccountCollection__Remove__Account() {
		return accountCollectionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClientCollection() {
		return clientCollectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClientCollection_Clients() {
		return (EReference)clientCollectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getClientCollection__Add__Client() {
		return clientCollectionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getClientCollection__Remove__Client() {
		return clientCollectionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClient() {
		return clientEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClient_TotalBalance() {
		return (EAttribute)clientEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClient_Accounts() {
		return (EReference)clientEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClient_Client_id() {
		return (EAttribute)clientEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClient_Name() {
		return (EAttribute)clientEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClient_Address() {
		return (EAttribute)clientEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClient_DateOfBirth() {
		return (EAttribute)clientEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getClient__AddAccount__Account() {
		return clientEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getClient__RemoveAccount__Account() {
		return clientEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAccount() {
		return accountEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAccount_AccountNumber() {
		return (EAttribute)accountEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAccount_AvaliableBalance() {
		return (EAttribute)accountEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccount__Credit__double() {
		return accountEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAccount__Debit__double() {
		return accountEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSavingAccount() {
		return savingAccountEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSavingAccount_InterestRate() {
		return (EAttribute)savingAccountEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSavingAccount__InterestRateCalculation() {
		return savingAccountEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCurrentAccount() {
		return currentAccountEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BankFactory getBankFactory() {
		return (BankFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		banksEClass = createEClass(BANKS);
		createEAttribute(banksEClass, BANKS__NAME);
		createEAttribute(banksEClass, BANKS__ADDRESS);
		createEReference(banksEClass, BANKS__ACCOUNT_COLLECTION);
		createEReference(banksEClass, BANKS__CLIENT_COLLECTION);
		createEOperation(banksEClass, BANKS___ADD_CLIENT__CLIENT);
		createEOperation(banksEClass, BANKS___REMOVE_CLIENT__CLIENT);
		createEOperation(banksEClass, BANKS___ADD_ACCOUNT__ACCOUNT);
		createEOperation(banksEClass, BANKS___REMOVE_ACCOUNT__ACCOUNT);

		accountCollectionEClass = createEClass(ACCOUNT_COLLECTION);
		createEReference(accountCollectionEClass, ACCOUNT_COLLECTION__ACCOUNTS);
		createEOperation(accountCollectionEClass, ACCOUNT_COLLECTION___ADD__ACCOUNT);
		createEOperation(accountCollectionEClass, ACCOUNT_COLLECTION___REMOVE__ACCOUNT);

		clientCollectionEClass = createEClass(CLIENT_COLLECTION);
		createEReference(clientCollectionEClass, CLIENT_COLLECTION__CLIENTS);
		createEOperation(clientCollectionEClass, CLIENT_COLLECTION___ADD__CLIENT);
		createEOperation(clientCollectionEClass, CLIENT_COLLECTION___REMOVE__CLIENT);

		clientEClass = createEClass(CLIENT);
		createEAttribute(clientEClass, CLIENT__TOTAL_BALANCE);
		createEReference(clientEClass, CLIENT__ACCOUNTS);
		createEAttribute(clientEClass, CLIENT__CLIENT_ID);
		createEAttribute(clientEClass, CLIENT__NAME);
		createEAttribute(clientEClass, CLIENT__ADDRESS);
		createEAttribute(clientEClass, CLIENT__DATE_OF_BIRTH);
		createEOperation(clientEClass, CLIENT___ADD_ACCOUNT__ACCOUNT);
		createEOperation(clientEClass, CLIENT___REMOVE_ACCOUNT__ACCOUNT);

		accountEClass = createEClass(ACCOUNT);
		createEAttribute(accountEClass, ACCOUNT__ACCOUNT_NUMBER);
		createEAttribute(accountEClass, ACCOUNT__AVALIABLE_BALANCE);
		createEOperation(accountEClass, ACCOUNT___CREDIT__DOUBLE);
		createEOperation(accountEClass, ACCOUNT___DEBIT__DOUBLE);

		savingAccountEClass = createEClass(SAVING_ACCOUNT);
		createEAttribute(savingAccountEClass, SAVING_ACCOUNT__INTEREST_RATE);
		createEOperation(savingAccountEClass, SAVING_ACCOUNT___INTEREST_RATE_CALCULATION);

		currentAccountEClass = createEClass(CURRENT_ACCOUNT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		savingAccountEClass.getESuperTypes().add(this.getAccount());
		currentAccountEClass.getESuperTypes().add(this.getAccount());

		// Initialize classes, features, and operations; add parameters
		initEClass(banksEClass, Banks.class, "Banks", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBanks_Name(), theEcorePackage.getEString(), "name", null, 0, 1, Banks.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBanks_Address(), theEcorePackage.getEString(), "address", null, 0, 1, Banks.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBanks_AccountCollection(), this.getAccountCollection(), null, "accountCollection", null, 0, 1, Banks.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBanks_ClientCollection(), this.getClientCollection(), null, "clientCollection", null, 0, 1, Banks.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getBanks__AddClient__Client(), null, "addClient", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getClient(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getBanks__RemoveClient__Client(), null, "removeClient", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getClient(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getBanks__AddAccount__Account(), null, "addAccount", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAccount(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getBanks__RemoveAccount__Account(), null, "removeAccount", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAccount(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(accountCollectionEClass, AccountCollection.class, "AccountCollection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAccountCollection_Accounts(), this.getAccount(), null, "accounts", null, 0, -1, AccountCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getAccountCollection__Add__Account(), null, "add", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAccount(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAccountCollection__Remove__Account(), null, "remove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAccount(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(clientCollectionEClass, ClientCollection.class, "ClientCollection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getClientCollection_Clients(), this.getClient(), null, "clients", null, 0, -1, ClientCollection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getClientCollection__Add__Client(), null, "add", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getClient(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getClientCollection__Remove__Client(), null, "remove", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getClient(), "c", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(clientEClass, Client.class, "Client", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClient_TotalBalance(), theEcorePackage.getEDouble(), "totalBalance", null, 0, 1, Client.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClient_Accounts(), this.getAccount(), null, "accounts", null, 0, -1, Client.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClient_Client_id(), theEcorePackage.getEInt(), "client_id", null, 0, 1, Client.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClient_Name(), theEcorePackage.getEString(), "name", null, 0, 1, Client.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClient_Address(), theEcorePackage.getEString(), "address", null, 0, 1, Client.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClient_DateOfBirth(), theEcorePackage.getEDate(), "dateOfBirth", null, 0, 1, Client.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getClient__AddAccount__Account(), null, "addAccount", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAccount(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getClient__RemoveAccount__Account(), null, "removeAccount", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAccount(), "a", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(accountEClass, Account.class, "Account", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAccount_AccountNumber(), theEcorePackage.getEInt(), "accountNumber", null, 0, 1, Account.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAccount_AvaliableBalance(), theEcorePackage.getEDouble(), "avaliableBalance", null, 0, 1, Account.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getAccount__Credit__double(), null, "credit", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "amount", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAccount__Debit__double(), null, "debit", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "amount", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(savingAccountEClass, SavingAccount.class, "SavingAccount", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSavingAccount_InterestRate(), theEcorePackage.getEDouble(), "InterestRate", null, 0, 1, SavingAccount.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getSavingAccount__InterestRateCalculation(), theEcorePackage.getEDouble(), "interestRateCalculation", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(currentAccountEClass, CurrentAccount.class, "CurrentAccount", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //BankPackageImpl
