/**
 */
package bank.impl;

import bank.BankPackage;
import bank.CurrentAccount;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Current Account</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CurrentAccountImpl extends AccountImpl implements CurrentAccount {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CurrentAccountImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankPackage.Literals.CURRENT_ACCOUNT;
	}

} //CurrentAccountImpl
