/**
 */
package bank.impl;

import bank.Account;
import bank.BankPackage;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Account</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link bank.impl.AccountImpl#getAccountNumber <em>Account Number</em>}</li>
 *   <li>{@link bank.impl.AccountImpl#getAvaliableBalance <em>Avaliable Balance</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AccountImpl extends MinimalEObjectImpl.Container implements Account {
	/**
	 * The default value of the '{@link #getAccountNumber() <em>Account Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccountNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int ACCOUNT_NUMBER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAccountNumber() <em>Account Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccountNumber()
	 * @generated
	 * @ordered
	 */
	protected int accountNumber = ACCOUNT_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getAvaliableBalance() <em>Avaliable Balance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvaliableBalance()
	 * @generated
	 * @ordered
	 */
	protected static final double AVALIABLE_BALANCE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getAvaliableBalance() <em>Avaliable Balance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvaliableBalance()
	 * @generated
	 * @ordered
	 */
	protected double avaliableBalance = AVALIABLE_BALANCE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AccountImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankPackage.Literals.ACCOUNT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAccountNumber() {
		return accountNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccountNumber(int newAccountNumber) {
		int oldAccountNumber = accountNumber;
		accountNumber = newAccountNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankPackage.ACCOUNT__ACCOUNT_NUMBER, oldAccountNumber, accountNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getAvaliableBalance() {
		return avaliableBalance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvaliableBalance(double newAvaliableBalance) {
		double oldAvaliableBalance = avaliableBalance;
		avaliableBalance = newAvaliableBalance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankPackage.ACCOUNT__AVALIABLE_BALANCE, oldAvaliableBalance, avaliableBalance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void credit(double amount) {
		this.setAvaliableBalance(this.getAvaliableBalance() + amount);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void debit(double amount) {
		this.setAvaliableBalance(this.getAvaliableBalance() - amount);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BankPackage.ACCOUNT__ACCOUNT_NUMBER:
				return getAccountNumber();
			case BankPackage.ACCOUNT__AVALIABLE_BALANCE:
				return getAvaliableBalance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BankPackage.ACCOUNT__ACCOUNT_NUMBER:
				setAccountNumber((Integer)newValue);
				return;
			case BankPackage.ACCOUNT__AVALIABLE_BALANCE:
				setAvaliableBalance((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BankPackage.ACCOUNT__ACCOUNT_NUMBER:
				setAccountNumber(ACCOUNT_NUMBER_EDEFAULT);
				return;
			case BankPackage.ACCOUNT__AVALIABLE_BALANCE:
				setAvaliableBalance(AVALIABLE_BALANCE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BankPackage.ACCOUNT__ACCOUNT_NUMBER:
				return accountNumber != ACCOUNT_NUMBER_EDEFAULT;
			case BankPackage.ACCOUNT__AVALIABLE_BALANCE:
				return avaliableBalance != AVALIABLE_BALANCE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankPackage.ACCOUNT___CREDIT__DOUBLE:
				credit((Double)arguments.get(0));
				return null;
			case BankPackage.ACCOUNT___DEBIT__DOUBLE:
				debit((Double)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (accountNumber: ");
		result.append(accountNumber);
		result.append(", avaliableBalance: ");
		result.append(avaliableBalance);
		result.append(')');
		return result.toString();
	}

} //AccountImpl
