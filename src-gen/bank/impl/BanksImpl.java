/**
 */
package bank.impl;

import bank.Account;
import bank.AccountCollection;
import bank.BankPackage;
import bank.Banks;
import bank.Client;
import bank.ClientCollection;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Banks</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link bank.impl.BanksImpl#getName <em>Name</em>}</li>
 *   <li>{@link bank.impl.BanksImpl#getAddress <em>Address</em>}</li>
 *   <li>{@link bank.impl.BanksImpl#getAccountCollection <em>Account Collection</em>}</li>
 *   <li>{@link bank.impl.BanksImpl#getClientCollection <em>Client Collection</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BanksImpl extends MinimalEObjectImpl.Container implements Banks {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getAddress() <em>Address</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddress()
	 * @generated
	 * @ordered
	 */
	protected static final String ADDRESS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAddress() <em>Address</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddress()
	 * @generated
	 * @ordered
	 */
	protected String address = ADDRESS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAccountCollection() <em>Account Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccountCollection()
	 * @generated
	 * @ordered
	 */
	protected AccountCollection accountCollection;

	/**
	 * The cached value of the '{@link #getClientCollection() <em>Client Collection</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClientCollection()
	 * @generated
	 * @ordered
	 */
	protected ClientCollection clientCollection;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BanksImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BankPackage.Literals.BANKS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankPackage.BANKS__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAddress(String newAddress) {
		String oldAddress = address;
		address = newAddress;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankPackage.BANKS__ADDRESS, oldAddress, address));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AccountCollection getAccountCollection() {
		return accountCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccountCollection(AccountCollection newAccountCollection, NotificationChain msgs) {
		AccountCollection oldAccountCollection = accountCollection;
		accountCollection = newAccountCollection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BankPackage.BANKS__ACCOUNT_COLLECTION, oldAccountCollection, newAccountCollection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccountCollection(AccountCollection newAccountCollection) {
		if (newAccountCollection != accountCollection) {
			NotificationChain msgs = null;
			if (accountCollection != null)
				msgs = ((InternalEObject)accountCollection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BankPackage.BANKS__ACCOUNT_COLLECTION, null, msgs);
			if (newAccountCollection != null)
				msgs = ((InternalEObject)newAccountCollection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BankPackage.BANKS__ACCOUNT_COLLECTION, null, msgs);
			msgs = basicSetAccountCollection(newAccountCollection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankPackage.BANKS__ACCOUNT_COLLECTION, newAccountCollection, newAccountCollection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClientCollection getClientCollection() {
		return clientCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClientCollection(ClientCollection newClientCollection, NotificationChain msgs) {
		ClientCollection oldClientCollection = clientCollection;
		clientCollection = newClientCollection;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BankPackage.BANKS__CLIENT_COLLECTION, oldClientCollection, newClientCollection);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClientCollection(ClientCollection newClientCollection) {
		if (newClientCollection != clientCollection) {
			NotificationChain msgs = null;
			if (clientCollection != null)
				msgs = ((InternalEObject)clientCollection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BankPackage.BANKS__CLIENT_COLLECTION, null, msgs);
			if (newClientCollection != null)
				msgs = ((InternalEObject)newClientCollection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BankPackage.BANKS__CLIENT_COLLECTION, null, msgs);
			msgs = basicSetClientCollection(newClientCollection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BankPackage.BANKS__CLIENT_COLLECTION, newClientCollection, newClientCollection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addClient(Client c) {
		if(c != null) {
			this.clientCollection.add(c);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeClient(Client c) {
		if(c != null) {
			this.clientCollection.remove(c);
		}
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addAccount(Account a) {
		if(a != null) {
			this.accountCollection.add(a);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeAccount(Account a) {
		if(a != null) {
			this.accountCollection.remove(a);
		}
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BankPackage.BANKS__ACCOUNT_COLLECTION:
				return basicSetAccountCollection(null, msgs);
			case BankPackage.BANKS__CLIENT_COLLECTION:
				return basicSetClientCollection(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BankPackage.BANKS__NAME:
				return getName();
			case BankPackage.BANKS__ADDRESS:
				return getAddress();
			case BankPackage.BANKS__ACCOUNT_COLLECTION:
				return getAccountCollection();
			case BankPackage.BANKS__CLIENT_COLLECTION:
				return getClientCollection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BankPackage.BANKS__NAME:
				setName((String)newValue);
				return;
			case BankPackage.BANKS__ADDRESS:
				setAddress((String)newValue);
				return;
			case BankPackage.BANKS__ACCOUNT_COLLECTION:
				setAccountCollection((AccountCollection)newValue);
				return;
			case BankPackage.BANKS__CLIENT_COLLECTION:
				setClientCollection((ClientCollection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BankPackage.BANKS__NAME:
				setName(NAME_EDEFAULT);
				return;
			case BankPackage.BANKS__ADDRESS:
				setAddress(ADDRESS_EDEFAULT);
				return;
			case BankPackage.BANKS__ACCOUNT_COLLECTION:
				setAccountCollection((AccountCollection)null);
				return;
			case BankPackage.BANKS__CLIENT_COLLECTION:
				setClientCollection((ClientCollection)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BankPackage.BANKS__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case BankPackage.BANKS__ADDRESS:
				return ADDRESS_EDEFAULT == null ? address != null : !ADDRESS_EDEFAULT.equals(address);
			case BankPackage.BANKS__ACCOUNT_COLLECTION:
				return accountCollection != null;
			case BankPackage.BANKS__CLIENT_COLLECTION:
				return clientCollection != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BankPackage.BANKS___ADD_CLIENT__CLIENT:
				addClient((Client)arguments.get(0));
				return null;
			case BankPackage.BANKS___REMOVE_CLIENT__CLIENT:
				removeClient((Client)arguments.get(0));
				return null;
			case BankPackage.BANKS___ADD_ACCOUNT__ACCOUNT:
				addAccount((Account)arguments.get(0));
				return null;
			case BankPackage.BANKS___REMOVE_ACCOUNT__ACCOUNT:
				removeAccount((Account)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", address: ");
		result.append(address);
		result.append(')');
		return result.toString();
	}

} //BanksImpl
