/**
 */
package bank;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Client Collection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link bank.ClientCollection#getClients <em>Clients</em>}</li>
 * </ul>
 *
 * @see bank.BankPackage#getClientCollection()
 * @model
 * @generated
 */
public interface ClientCollection extends EObject {
	/**
	 * Returns the value of the '<em><b>Clients</b></em>' containment reference list.
	 * The list contents are of type {@link bank.Client}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Clients</em>' containment reference list.
	 * @see bank.BankPackage#getClientCollection_Clients()
	 * @model containment="true"
	 * @generated
	 */
	EList<Client> getClients();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void add(Client c);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model cUnique="false"
	 * @generated
	 */
	void remove(Client c);

} // ClientCollection
