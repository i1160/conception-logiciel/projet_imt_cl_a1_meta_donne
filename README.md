# IMT FIL-A1 - Projet CL | Modeling a Bank CRM (Customer Relationship Management) system in Xcore format and implementation of a program for handling the modeled system.

---

### Project presentation

Command line Bank CRM respecting CRUD system. It runs with Java JDK 16,
on Eclipse Modeling Tools (xcore and xmi).

A Customer Relationship Management solution in banking helps banks
manage customers and better understand their needs
in order to provide the right solutions, quickly.

You can Create, Read, Update and Delete Banks, Clients and Accounts.

An account is associated with a client. And all clients and accounts
are associated with bank (in account and client collection).
By modifying one of the components, the others also change.
(Chek the UML Diagram for more information)

#### Model UML :

- All classes depends on Bank class
- A Bank contains two collection classes : one to one
  - ClientCollection : can be assigned to several Clients - One to many and
    - Client class : can be assigned to several ClientCollection -
  - AccountCollection : can be assigned to several Accounts - One to many
    - Accounts class : can be assigned to several Clients & AccountCollection -

![Alt text](./resources_doc/UML_bank.png "UML Bank")

#### Run the application
/!\ Make sur to not change the file `bank.xcore` or rebuild the project.
Beacause some of methodes in the classes are re-edited after auto-generation of classes.

- When you first run the application you have the following choices:

```bash
Welcome to <Bank_name> !
 Pease chose one of these option to continu :
1 - Create a new Bank.
2 - Load a existing Bank.
3 - Exit the interface
```

You have choice to create a new Bank or upload a Bank (.xmi file)

- A exemple of a bank is stored in resources_doc : `example_bank.xmi`

  - Use the following path to upload the bank : `resources_doc/example_bank.xmi`

- After creating or uploading a Bank, you can go to client management
  section or account management section :

```bash
Welcome to <Bank_name> !
 Pease chose one of these option to continu :
1 - Go to Clients section :
2 - Go to Accounts section  :
3 - Exit & save !:
```

##### Client section :

You have following choices in Client section, depending on
which choice you choose several informations are displayed

```bash
Welcome to <Bank_name> !
 Pease chose one of these option to continu :
1 - Create a new Client :
2 - Read all informations of existing a Client :
3 - Update a existing [update info, credit or debit account] Client :
4 - Delete a existing Client :
5 - Get list of all Clients :
6 - Exit & save !
```

##### Account section :

You have following choices in Account section, depending on
which choice you choose several informations are displayed

```bash
Welcome to <Bank_name> !
 Pease chose one of these option to continu :
1 - Create a new Account :
2 - Read all informations of existing a Account :
3 - Update a existing [update info, credit or debit account] Account :
4 - Delete a existing Account :
5 - Get list of all Accounts :
6 - Exit & save !
```
##### Save model in to a .xmi file

After all operations you can save the Bank model in a .xmi file by giving a path in option 
```bash
Saving bank information ...
Enter the file name in the following format:  filename.xmi
```
