package services;

import bank.BankFactory;
import bank.Banks;
import tools.Menu;
import tools.ResourceManager;
import org.eclipse.emf.ecore.resource.Resource;


public class ServiceMain {
	private String filename;

	public ResourceManager rs;
	public BankFactory bankFactory;
	private Banks bank;
	
	public ServiceMain(ResourceManager res_,BankFactory bank_Fact) {
		this.rs = res_;
		this.bankFactory = bank_Fact;
		this.filename = "";
	}
	
	public void mainCRM() {
		boolean end = false;
		while(!end) {
			Menu.startMenuBank();
			
			switch(Menu.getInput()){
				case "1":{
					createBank();
					break;
				}
				case "2":{
					loadBank();
					break;
				}
				case "3":{
					end =  true;
					break;
				}
				default:
					throw new IllegalArgumentException("The option you chosed doesn't exist :(, please try again:");
			
			}
		}
		System.out.println("Ending program ...");
		System.exit(0);
		System.out.println("End!");

	}

	private void createBank() {
		this.bank = this.bankFactory.createBanks();
		
		System.out.print("Enter the Bank name :");
		String name = Menu.getInput();
		this.bank.setName(name);
		System.out.print("Enter the Bank adress :");
		String adress = Menu.getInput();
		this.bank.setAddress(adress);
		
		this.saveIntoResource();
		this.manageBank();
		
	}
	
	private void manageBank() {
		String choice;
		
		ServiceBank newBankS = new ServiceBank(this.bank, this.bankFactory); 
		
		Menu.menuGoToSection(this.bank.getName());
		choice = Menu.getInput();
		
		while(newBankS.manageBankMain(choice)) {
			Menu.menuGoToSection(this.bank.getName());
			choice = Menu.getInput();
		}
		System.out.println("Saving bank information ...");
		this.saveIntoResource();
	}
	
	private void loadBank() {
		Menu.menuFileCreation("path");
		
		try {
			this.filename = Menu.getInput().trim();
			Resource rsNew = this.rs.getResource(this.filename);
			this.bank = (Banks)(rsNew.getContents().get(0));
			manageBank();
		}catch(Exception e) {
			throw new IllegalArgumentException("Unable to find the file !");
		}
		
	}

	private void saveIntoResource() {
		Menu.menuFileCreation("name");
		this.rs.store(Menu.getInput(), this.bank);
	}

	

	
	
}
