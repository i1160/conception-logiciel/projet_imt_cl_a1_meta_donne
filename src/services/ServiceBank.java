package services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.UUID;

import org.eclipse.emf.common.util.EList;

import bank.Account;
import bank.BankFactory;
import bank.Banks;
import bank.Client;
import bank.CurrentAccount;
import tools.Menu;
import tools.SequenceAccount;
import tools.SequenceClient;

public class ServiceBank {
	private Banks bank;
	public BankFactory bankFactory;


	public ServiceBank(Banks bank, BankFactory bankFactory) {
		this.bank = bank;
		this.bankFactory = bankFactory;
		if(this.bank.getAccountCollection() == null)
			this.bank.setAccountCollection(this.bankFactory.createAccountCollection());
		if(this.bank.getClientCollection() == null)
			this.bank.setClientCollection(this.bankFactory.createClientCollection());
	}

	public boolean manageBankMain(String input) {
		boolean process = true;
		switch(input){
		case "1":{
			Menu.menuStartClient();
			this.manageClient(Menu.getInput());
			break;
		}
		case "2":{
			Menu.menuStartAcount();
			this.manageAccount(Menu.getInput());
			break;
		}
		case "3":{
			process =  false;
			break;
		}
		default:
			throw new IllegalArgumentException("The option you chosed doesn't exist :(, please try again:");

		}
		return process;
	}


	public void manageClient(String userInput) {
		switch(userInput){
		case "1":{
			boolean chosen = false;
			Client newClient = this.bankFactory.createClient();
			newClient.setClient_id(SequenceClient.nextValue());

			System.out.println("Enter Client name :");
			String name =  Menu.getInput();
			newClient.setName(name);

			System.out.println("Enter Client adress :");
			String address =  Menu.getInput();
			newClient.setAddress(address);

			System.out.println("Enter Client date of birth in the following format JJ-MM-YYYY:");

			try {
				Date dateOfBirth = new SimpleDateFormat("dd-MM-yyyy").parse(Menu.getInput());
				Instant instant = dateOfBirth.toInstant();
				ZonedDateTime zone = instant.atZone(ZoneId.systemDefault());
				LocalDate givenDate = zone.toLocalDate();
				Period period = Period.between(givenDate, LocalDate.now());
				if(period.getYears() >= 18) {
					newClient.setDateOfBirth(dateOfBirth);
				}else {
					System.out.println("Client is under 18 yers old, the client can't be register !");
					break;
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Menu.menuTypeOfAccount();

			while(!chosen) {
				switch(Menu.getInput()) {
				case "1":{
					creatAccountForClient("", newClient);
					chosen = true;
					break;
				}
				case "2":{
					creatAccountForClient("SAVING", newClient);
					chosen = true;
					break;
				}
				default:
					throw new IllegalArgumentException("The option you chosed doesn't exist :(, please try again:");

				}
			}
			this.bank.addClient(newClient);
			System.out.println("Client register to the bank " + this.bank.getName() + " !");

			break;
		}
		case "2":{
			boolean existClient = false;
			while(!existClient) {
				System.out.println("Enter Client name :");
				String nameClient =  Menu.getInput();


				if(this.bank.getClientCollection().getClients().size() != 0) {
					for (Client client : this.bank.getClientCollection().getClients()) {
						if(client.getName().equals(nameClient.trim())) { 
							System.out.println("Client info : | Id | Name | Address | Total Balance");
							System.out.println(client.getClient_id() + " | " + client.getName() + " | " + client.getAddress() + " | " + client.getTotalBalance() + "�" );

							for (Account account : client.getAccounts())
								account.toString();
							existClient =  true;
							break;
						}
					}

					if(!existClient) {
						System.out.println("Client don't exist");
						System.out.println("Write 1 for exit :");
						String retryClientName = Menu.getInput();
						if(retryClientName.equals("1")) {
							existClient =  true;
						}

					}
				}else {
					System.out.println("There is no cient in DB!");
					break;
				}

			}
			break;
		}
		case "3":{
			Menu.menuUpdate();

			boolean chosen = false;
			while(!chosen ) {
				switch(Menu.getInput()) {
				case "1":{
					System.out.println("You can update adress's client");
					boolean existClient = false;
					while(!existClient) {
						System.out.println("Enter Client name to update his address :");
						String nameClient =  Menu.getInput();
						System.out.println("Enter the new adress :");
						String newAdressClient = Menu.getInput();

						for (Client client : this.bank.getClientCollection().getClients()) {
							if(client.getName().equals(nameClient)) { 
								existClient =  true;
								client.setAddress(newAdressClient);
								chosen = true;
								break;
							}
						}
					}

					if(!existClient) {
						System.out.println("Client don't exist");
						System.out.println("Write 1 for exit :");
						String retryClientName = Menu.getInput();
						if(retryClientName.equals("1")) {
							existClient =  true;
						}

					}

					break;
				}
				case "2":{

					boolean existClient = false;
					while(!existClient) {
						System.out.println("Enter Client name to credit his account :");
						String nameClient =  Menu.getInput();
						System.out.println("Enter a amount to credit :");
						double creditClient =  Double.parseDouble(Menu.getInput());

						// get client
						for (Client client : this.bank.getClientCollection().getClients()) {
							if(client.getName().equals(nameClient.trim())) { 
								existClient =  true;
								//get account's client
								EList<Account> accounts = client.getAccounts();
								System.out.println("Chose a account number :");
								for (Account account : accounts) {
									System.out.println(account.toString());

								}
								int accountNumber = Integer.parseInt(Menu.getInput());
								// update credit acccount
								for (Account account : accounts) {
									if(account.getAccountNumber() == accountNumber) {
										account.credit(creditClient);
										for(Account a: this.bank.getAccountCollection().getAccounts()) {
											if(a.getAccountNumber() == account.getAccountNumber())
												a.credit(creditClient);
										}
										System.out.println("Credit done !");
										System.out.println("New balance : " + client.getTotalBalance());
										chosen = true;
									}

								}
								break;
							}

						}
					}

					if(!existClient) {
						System.out.println("Client don't exist");
						System.out.println("Write 1 for exit :");
						String retryClientName = Menu.getInput();
						if(retryClientName.equals("1")) {
							existClient =  true;
						}

					}

					break;
				}
				case "3" :{
					boolean existClient = false;
					while(!existClient) {
						System.out.println("Enter Client name :");
						String nameClient =  Menu.getInput();
						System.out.println("Enter a amount to debit :");
						double debitClient =  Double.parseDouble(Menu.getInput());

						// get client
						for (Client client : this.bank.getClientCollection().getClients()) {
							if(client.getName().equals(nameClient)) { 
								existClient =  true;
								//get account's client
								EList<Account> accounts = client.getAccounts();
								System.out.println("chose a account number :");
								for (Account account : accounts) {
									System.out.println(account.toString());
								}
								int accountNumber = Integer.parseInt(Menu.getInput());
								// update debit acccount
								for (Account account : accounts) {
									if(account.getAccountNumber() == accountNumber) {
										account.debit(debitClient);
										for(Account a: this.bank.getAccountCollection().getAccounts()) {
											if(a.getAccountNumber() == account.getAccountNumber())
												a.debit(accountNumber);
										}
										System.out.println("debit done !");
										chosen = true;
									}

								}
								break;
							}

						}
					}

					if(!existClient) {
						System.out.println("Client don't exist");
						System.out.println("Write 1 for exit :");
						String retryClientName = Menu.getInput();
						if(retryClientName.equals("1")) {
							existClient =  true;
						}

					}

					break;


				}
				default:
					throw new IllegalArgumentException("The option you chosed doesn't exist :(, please try again:");

				}


			}

			break;
		}
		case "4":{
			boolean notExistClient = true;
			while(notExistClient) {
				System.out.println("Enter a Client name :");
				String nameClient =  Menu.getInput();

				for (Client client : this.bank.getClientCollection().getClients()) {
					if(client.getName().equals(nameClient)) { 
						// Delete all client accounts 
						for(Account clientA : client.getAccounts()) {
							for(Account a: this.bank.getAccountCollection().getAccounts()) {
								if(a.getAccountNumber() == clientA.getAccountNumber())
									this.bank.getAccountCollection().remove(a);
							}
						}
						this.bank.getClientCollection().getClients().remove(client);
						notExistClient =  false;
						System.out.println("Client was remove");
						break;
					}
				}


				if(notExistClient) {
					System.out.println("Client don't exist");
					System.out.println("Write 1 for exit :");
					String retryClientName = Menu.getInput();
					if(retryClientName.equals("1")) {
						notExistClient =  true;
					}

				}

			}
			break;
		}
		case "5":{
			System.out.println();
			System.out.println("List of all clients : | Id | Name | Address | Total Balance");
			int i = 0;
			for(Client c : this.bank.getClientCollection().getClients()) {
				i += 1;
				System.out.println(i + ". | " + c.getClient_id() + " | " + c.getName() + " | " + c.getAddress() + " | " + c.getTotalBalance() + "�" );
				System.out.println("List of "+ c.getName() + " accounts : Account(s) N� | Account type | Account(s) balance");
				for(Account a : c.getAccounts()) {
					System.out.println(a.getAccountNumber() + " | " + a.getClass().getSimpleName() + " | " + a.getAvaliableBalance());
				}
				System.out.println();
			}
			break;
		}
		case "6":{

			break;
		}

		default:
			throw new IllegalArgumentException("The option you chosed doesn't exist :(, please try again:");

		}
	}


	public void manageAccount( String userInput) {
		switch(userInput){
		case "1":{
			int i = 0;

			System.out.println();
			if(this.bank.getClientCollection().getClients().size() == 0) {
				System.out.println("No client Data in the bank ! Please first creat a client before creating an account");
				break;
			}

			System.out.println("Chose a client id for with you wants to creat a new account, from the following list :");
			for(Client allClients: this.bank.getClientCollection().getClients()) {
				i += 1;
				System.out.println(1 + ". | Id:" + allClients.getClient_id() +" - Name: " + allClients.getName());
			}
			int client_id =  Integer.parseInt(Menu.getInput());

			System.out.println();
			Menu.menuTypeOfAccount();
			String accountType = Menu.getInput();

			if(accountType.equals("1")) {
				this.creatAccount("", client_id);
				break;
			}else if(accountType.equals("2")) {
				this.creatAccount("SAVING", client_id);
				break;
			}else {
				System.out.println("Giving choice didn't matched with the given options, going back to the mainmenu :(");
				break;
			}		
		}
		case "2":{
			boolean accountFind =false;
			System.out.println();
			if(this.bank.getClientCollection().getClients().size() == 0) {
				System.out.println("No client Data in the bank ! Please first creat a client before using an account");
				break;
			}
			System.out.println("Chose one account, there is the list of all existing accounts :");
			for(Account account : this.bank.getAccountCollection().getAccounts()) {
				System.out.println("account number " + account.getAccountNumber()  + " its balance : " + account.getAvaliableBalance());

			}
			int numberAccount = Integer.parseInt(Menu.getInput());
			for(Client c : this.bank.getClientCollection().getClients()) {
				for(Account clientAccounts : c.getAccounts()) {
					if(clientAccounts.getAccountNumber() == numberAccount) {
						accountFind = true;

						System.out.println( ". | " + clientAccounts.getAccountNumber() + " | " + clientAccounts.getAvaliableBalance() + " |  " + c.getName() + "|" + c.getAddress() + "|" + c.getDateOfBirth());
					}
				}
			}

			System.out.println();
			if(!accountFind)
				System.out.println("Giving not existing account, going back to the main menu");


			break;
		}
		case "3":{
			System.out.println("Case to implement - Same as Client update !");
			// Same as client section
			// TODO : Update a account by a given client id: debit or credit --> update also client account and account in Bank accountCollection.
			break;
		}
		case "4":{
			boolean accountFind =false;
			if(this.bank.getClientCollection().getClients().size() == 0) {
				System.out.println("No client Data in the bank ! Please first creat a client before using an account");
				break;
			}
			System.out.println("Chose one account, there is the list of all existing accounts :");
			for(Account account : this.bank.getAccountCollection().getAccounts()) {
				System.out.println("account number " + account.getAccountNumber()  + " its balance : " + account.getAvaliableBalance());

			}
			int numberAccount = Integer.parseInt(Menu.getInput());
			for(Account a : this.bank.getAccountCollection().getAccounts()) {

				for(Client c : this.bank.getClientCollection().getClients()) {
					for(Account clientAccounts : c.getAccounts()) {
						if(clientAccounts.getAccountNumber() == numberAccount) {
							accountFind = true;
							c.removeAccount(clientAccounts);
							this.bank.getAccountCollection().remove(clientAccounts);
							System.out.println("account deleted !");

						}
					}
				}

				System.out.println();
			}
			if(!accountFind)
				System.out.println("Giving not existing account, going back to the main menu");


			break;
		}
		case "5":{
			System.out.println();
			System.out.println("List of all accounts : | Id | Avaliable Balance | Account type | Associated Client");
			System.out.println();
			int i = 0;
			for(Client c : this.bank.getClientCollection().getClients()) {
				for(Account clintAccounts : c.getAccounts()) {
					System.out.println(i + ". | " + clintAccounts.getAccountNumber() + " | "  + clintAccounts.getClass().getSimpleName() +" | "+ clintAccounts.getAvaliableBalance() + " | " + c.getName());
				}
			}
			/*for(Account a : this.bank.getAccountCollection().getAccounts()) {
				i += 1;
				

				System.out.println();
			}*/
			break;
		}
		case "6":{
			break;
		}

		default:
			throw new IllegalArgumentException("The option you chosed doesn't exist :(, please try again:");

		}

	}

	public void creatAccountForClient(String type, Client client) {
		Account newSimpleAccount = null;
		Account newSimpleAccountBank = null;

		if(type.equals("SAVING")) {
			newSimpleAccount = this.bankFactory.createSavingAccount();
			newSimpleAccountBank = this.bankFactory.createSavingAccount();
		}else {
			newSimpleAccount = this.bankFactory.createCurrentAccount();
			newSimpleAccountBank = this.bankFactory.createCurrentAccount();
		}	
		
		System.out.println("Setting account param...");
		newSimpleAccount.setAccountNumber(SequenceAccount.nextValue());
		newSimpleAccountBank.setAccountNumber(newSimpleAccount.getAccountNumber());

		newSimpleAccount.setAvaliableBalance(0);
		newSimpleAccountBank.setAvaliableBalance(0);

		// Account association to client & bank
		client.setTotalBalance(0);
		
		System.out.println("Associating account to the Bank and Client...");
		client.addAccount(newSimpleAccount);
		this.bank.addAccount(newSimpleAccountBank);

		System.out.println("New account was created for " + client.getName() + " with 0� as account balance !");

	}

	public void creatAccount(String type, int client_id) {
		Account newAccountClient = null;
		Account newAccountBank = null;
		if(type.equals("SAVING")) {
			newAccountClient = this.bankFactory.createSavingAccount();
			newAccountBank = this.bankFactory.createSavingAccount();
		}else {
			newAccountClient = this.bankFactory.createCurrentAccount();
			newAccountBank = this.bankFactory.createCurrentAccount();
		}

		System.out.println();
		System.out.println("Setting account params, by default account amount is 0� !");


		newAccountClient.setAccountNumber(this.bank.getAccountCollection().getAccounts().size() + 1);
		newAccountBank.setAccountNumber(newAccountClient.getAccountNumber());

		newAccountClient.setAvaliableBalance(0);
		newAccountBank.setAvaliableBalance(0);

		System.out.println("Account created !");
		System.out.println();
		System.out.println("Associating new account to the client and bank...");
		System.out.println(this.bank.getName());
		for(Client updateClient: this.bank.getClientCollection().getClients()) {
			if(updateClient.getClient_id() == client_id) {
				updateClient.addAccount(newAccountClient);
			}
		}

		this.bank.addAccount(newAccountBank);

		System.out.println("Account associated to client and Bank !");
	}

}
