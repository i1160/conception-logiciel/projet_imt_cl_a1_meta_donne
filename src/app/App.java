package app;

import bank.BankFactory;
import services.ServiceMain;
import tools.ResourceManager;

public class App {

	public static void main(String[] args) {
		BankFactory bankFactory = BankFactory.eINSTANCE;
		ResourceManager rs = new ResourceManager();
		
		ServiceMain newService = new ServiceMain(rs,bankFactory);
		newService.mainCRM();
	}

}
