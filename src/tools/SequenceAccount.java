package tools;

import java.util.concurrent.atomic.AtomicInteger;

public class SequenceAccount {
	  private static final AtomicInteger counter = new AtomicInteger();

	  public static int nextValue() {
	    return counter.getAndIncrement() + 1;
	  }
}
