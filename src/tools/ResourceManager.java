package tools;

import java.io.IOException;
import java.util.Collections;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import bank.BankPackage;
import bank.Banks;


public class ResourceManager {
	ResourceSet resourceS;

	public ResourceManager() {
		this.resourceS =  new ResourceSetImpl();
		resourceS.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());

		// Saving Bank model
		this.resourceS.getPackageRegistry().put(BankPackage.eNS_URI, BankPackage.eINSTANCE);
	}

	public Resource getResource(String filename) {
		return this.resourceS.getResource(URI.createFileURI(filename), true);
	}

	public void store(String filename, Banks racineModele) {
		// Creation of resource to store data
		Resource resource = this.resourceS.createResource(URI.createFileURI(filename));
		// Adding model

		resource.getContents().add(racineModele);

		try {
			resource.save(Collections.EMPTY_MAP);
		}
		catch (IOException e){
			System.out.println("Error : Enable to save the model, details :" + e.getMessage());
		}

	}




}
