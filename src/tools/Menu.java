package tools;

import java.util.Scanner;

public class Menu {

	public static void startMenuBank() {
		System.out.println("Welcome to IMT Bank CRM ! \n Pease chose one of these option to continu : ");
		System.out.println("1 - Create a new Bank.");
		System.out.println("2 - Load a existing Bank.");
		System.out.println("3 - Exit the interface");
	}

	public static void menuGoToSection(String bank_name) {
		System.out.println("Welcome to " +bank_name + "! \n Pease chose one of these option to continu : ");
		System.out.println("1 - Go to Clients section :");
		System.out.println("2 - Go to Accounts section  :");
		System.out.println("3 - Exit & save !:");
	}

	public static void menuStartClient() {
		messageCRUDParser("Client");
	}

	public static void menuStartAcount() {
		messageCRUDParser("Account");
	}

	public static void messageCRUDParser(String obj) {
		System.out.println("Welcome to IMT Bank CRM Account" + obj + "s section ! \n Pease chose one of these option to continu : ");
		System.out.println("1 - Create a new "+ obj + " :");
		System.out.println("2 - Read all informations of existing a "+ obj +" :");
		System.out.println("3 - Update a existing [update info, credit or debit account] "+ obj +" :");
		System.out.println("4 - Delete a existing " + obj + " :");
		System.out.println("5 - Get list of all "+ obj +"s :");
		System.out.println("6 - Exit & save !");

	}

	public static void menuTypeOfAccount() {
		System.out.println("Please chose the type of acccount to create :");
		System.out.println("1 - Create a Normal account.");
		System.out.println("2 - Create a Saving account.");
	}

	public static void menuFileCreation(String option) {
		if(option.equals("name")) {
			System.out.println("Enter the file name in the following format:  filename.xmi");
		}else {
			System.out.println("Give the path to acces the model : ");
		}
	}

	public static String getInput() {
		Scanner scan = new Scanner(System.in);
		return scan.nextLine();
	}

	public static void menuUpdate() {
		System.out.println("Which updating you want to do ?");
		System.out.println("1 - Update a information about a account.");
		System.out.println("2 - Credit account.");
		System.out.println("3 - Debit account.");

	}
}
